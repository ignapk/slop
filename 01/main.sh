#!/usr/bin/bash
#
## Komentarz
#
## Deklaracja zmiennych
#
a="Hello there"

echo $a

echo

#apostrofy
echo 'Tekst: $a'
echo "Tekst: $a"
echo "Tekst: \$a"

echo

echo `ls -a`

#wersja alternatywna
echo $( ls -a )

echo

# Rodzaje zmiennycha
# Zmienne lokalne
b="Zmienna lokalna"

katalog=$( pwd )
echo "Jestes w katalogu: $katalog"

katalog=`pwd`
echo "Jestes w katalogu: $katalog"

#Zmienne specjalne
echo "$0" #nazwa skryptu
echo "$@" #wszystkie argumenty

echo

echo "$?" #czy udalo sie
echo "$$" #pid powloki

#Zmienne srodowiskowe

export a #sprawia, ze podpowloki widza $a
export -n a #przestaje byc globalna
export -p #wysweitl wszystkie

echo $( env )

#zmienne tablicowe
tablica=(1 2 3 4 5)
echo ${tablica[0]}
echo ${tablica[*]} #wszystkie
echo ${tablica[@]} #wszystkie

echo ${#tablica[0]} #liczba znakow elementu

# dodawanie elementów do tablicy
tablica[5]=6
echo ${tablica[*]}

tab[0]="Tak"
tab[1]="Nie"

echo ${tab[@]}

echo

tablica=(1 2 3 4 5)

#usuwanie elementów tablicy
unset tablica[3]
echo ${tablica[@]}

#usuwanie całej tablicy
unset tablica[*]
echo ${tablica[@]}
