#include <stdio.h>
#include <signal.h>

void sig_handler (int signum)
{
	switch (signum)
	{
		case SIGTERM:
			printf ("I'm still standing, better than I ever did!\n");
			break;
		case SIGTSTP:
			printf ("Don't stop me now, cause I'm having a good time!\n");
			break;
		case SIGINT:
			printf ("Process, Interrupted.");
			break;
		default:
			break;
	}
}

int main ()
{
	signal (SIGTERM, sig_handler);
	signal (SIGTSTP, sig_handler);
	signal (SIGINT, sig_handler);
	while (1);
	return 0;
}
