#!/usr/bin/bash


<<COSTAM
Deskryptory


0 - Deskryptor wejściowy
1 - Deskryptor wyjściowy
2 - Deskryptor wyjściowy błędu

COSTAM

# Przyklad1 (utworzenie pliku z zawartoscia)
ls > plik.txt

# Przyklad2 (utworzenie pliku z zawartoscia wykorzystujac stdin)
#exec 1> "plik1.txt"
#ls >&1 #ampersand bo inaczej poszlo by do pliku 1

# Przyklad3 (Polaczenie 2 plikow w calosc"
echo "Witaj w Slackware" > plik1.txt
echo "Powtorka z Bash" > plik2.txt
cat plik1.txt plik2.txt > plik12.txt

# Przykald4 (Dopisywanie danych do pliku)
echo "Cwiczenia" >> plik12.txt

# Przykald5 (Przekierowanie strumenia)
#exec 2>&1
echo "Testujemy" > /dev/null #>&2
#echo "Testujemy" > plik #>&2

# Przyklad6
ls -a | grep ".txt"

# Przyklad7 (potoki nazwane)
mkfifo myPipe
ls -al > myPipe
#grep ".txt" < myPipe
