#!/usr/bin/awk -f

BEGIN { print FS = "," }

#wyswietlanie wszystkich linii
#{ print $0 }

#wyswietlanie 1 kolumy
#{ print $1 }

#wyswietlanie 1 i 3 kolumhh
#{ print $1 "/t" $3 }

#wyswietlanie w roznej kolejnosci
#{ print $3 "/t" $1 }

#wyswietlanei liniii, ktore spelniajai wzorzec
#/female/ { print $0 }
#/some hi*/ { print $0 }

#liczejnie wierszy, ktore spelniaja wzorzec
#/female/ { ++ile }
#END { print "Ilosc = ", ile }

#wysweitlanie linii, ktore maja okreslona liczbe znakow
length($0) < 68
