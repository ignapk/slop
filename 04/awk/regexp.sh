#!/usr/bin/bash

#Przyklad1
echo -e "kot\nplot\npot\npit\npat" | awk "/p.t/"

echo

#Przyklad2
echo -e "Test\nTenis\nTeraz\nTenor" | awk "/^Ten/"

echo

#Przyklad3
echo -e "kot\nplot\npot\npit\npat" | awk "/ot$/"

echo

#Przyklad4
echo -e "kot\nlot\npot\npit\npat" | awk "/[pl]ot/" 

echo

#Przyklad5
echo -e "kot\nlot\npot\npit\npat" | awk "/[^pl]ot/"

echo

#przyklad6
echo -e "kot\nlot\npot\npit\npat" | awk "/pot|lot/"

echo

#Przyklda 7
echo -e "sto\nstos" | awk "/stos?/"

echo

#Przyklad 8
echo -e "sto\nstop\nstopp" | awk "/stop*/"

#Przyklad9
echo -e "112\n242\n123\n331\n456\n222" | awk "/2+/"

echo

#Przyklad10
echo -e "Nowy traktor\nNowy rower\nNowy samochod\nNowy kombajn" | awk "/Nowy (taktor|kombajn)/"
