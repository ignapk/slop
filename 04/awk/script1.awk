#!/usr/bin/awk -f

#SkLadnia awk
#[wzorzec] <program>

#$0 - cały wiersz, $1 - 1 grupa, 2 - 2 grupa itp.

#Przykład 1 (wypisanie wszystkich imion konczacyh sie na asia)
/asia/ { print $0 }

#Przykald 2 (wszystkie osoby, ktore maja na imie zosia)
/^Zosia/ { print "Witaj " $0 }
/^Zosia/ { print "Witaj " $1 }
