#!/usr/bin/awk -f

#ARGC - ilosc parametrow
#BEGIN { print "Parametry = ", ARGC }

#ARGV
BEGIN {
	for ( i = 0; i < ARGC; ++i ) {
		printf "ARGV[%d] = %s\n", i, ARGV[i]
	}
}

#CONVFMT - format liczby
BEGIN { print "Format liczby = ", CONVFMT }

#ENVIRON - tablica zmiennych srodowiskowych
BEGIN { print ENVIRON["USER"] }

#FILENAME - nazwa pliku (parametr) - tylko blok END
END { print FILENAME }

#NF - ilosc kolumn
BEGIN {}
NF > 6

#NR - ilosc kolumn w aktualnym wierszu
BEGIN {}
NR > 6

#FNR - podobny do NR ale uzyteczny w wielu plikach
#Resetuje wartość gdy nowy plik

#OFMT - format wyjściowy liczby
BEGIN { print "OFMT = ", OFMT }

#OFS - separator wyjściowy dla pól (kolumn). Domyślnie spacja
BEGIN { print "OFS = ", OFS }

#ORS = separator wyjściowy dla rekordów (wierszy). Domyślnie nowa linia
BEGIN { print "ORS = ", ORS }

#RLENGTH - długość stringa dopasawan przez funkcję match
BEGIN { if ( match("czerwony niebieski zielony", "ony")) { print RLENGTH } }

#RS - separator wejściowy dla rekordów. Wartość domyślna to nowa linia
BEGIN { print "RS = ", RS }

#RSTART -  pierwsza pozycja stringa dopasowanego przez funkcje match
BEGIN { if ( match("czerwony niebieski zielony", "zie")) { print RSTART } }

#SUBSEP - znak separatora dla indesków tablic
BEGIN { print "SUBSEP = ", SUBSEP }
