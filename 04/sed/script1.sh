#!/usr/bin/bash

<<KOMENTARZ

Paramtetry sed:
-n - na wyjściu będą wypisywanie jedynie linie, na których wykonano
     komendę p lub s z parametrem p
-e - wiele polecen
-f wczytywanie z pliku
-v info o programie
-t wylaczenie wyjscia z istotnych komend (is it really?)
-q bez ostrzezen o rezultatach
-i edycja w miejscu
-b backup (is it really? or rather shouldnt it be -i.bak for backup)

Składnia sed
sed "s/stary/nowy" plik

KOMENTARZ

#Przyklad1 usuwanie linii

sed -e "1d" -e "2d" -e "7d" ksiazki2.txt

echo

#Przyklad2 - zastapienie wyrazu1 przez wyraz2 (pierwsze wystapienie)
sed "s/Proces/Wyrok/" ksiazki2.txt

echo

#Przyklad3 - zastąpienie wyraz1 przez wyraz2 (globalnie)
sed "s/Proces/Wyrok/g" ksiazki2.txt

echo
echo
echo

#Przyklad4 - zastąpienie wielu wyrazów
sed "s/Proces/Wyrok/" ksiazki2.txt > ksiazki2m.txt | sed "s/Mały/Duży/" ksiazki2m.txt
echo
sed "s/Proces/Wyrok/" ksiazki2.txt > ksiazki2m.txt && sed "s/Mały/Duży/" ksiazki2m.txt


#Przyklad5 - zastąpienie wielu wyrazów - metoda 2
sed "s/Proces/Wyrok/;s/Mały/Duży/" ksiazki2.txt
