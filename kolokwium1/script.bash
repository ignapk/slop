#!/usr/bin/bash

#1)
osoby=$(awk -F";" '/^R/ { if ($2 < 10) print $0 }' dane_kolokwium.csv)

echo "$osoby"

ilosc=$(echo "$osoby" | awk '{ count++ } END { print count }')

echo "$ilosc"

#2)
min_wiek=$(echo "$osoby" | awk -F";" 'BEGIN { min_wiek = 100 } { if ($2 < min_wiek) min_wiek = $2 } END { print min_wiek }')

osoby_zamienione=$(echo "$osoby" | sed "s/$min_wiek/100/g")

#3)
suma_wieku=$(echo "$osoby" | awk -F";" '{ sum += $2 } END { print sum }')

srednia=$(echo "$suma_wieku / $ilosc" | bc -l)

#4)
echo "$osoby
$ilosc
$osoby_zamienione
$srednia
" > tmp

dialog --textbox tmp 40 80
rm tmp
