#!/usr/bin/bash

exec 1>plik.txt

echo "Informacja o procesorze: "
cat /proc/cpuinfo
echo

echo "Informacja o jądrze: "
uname -a
echo

echo "Nazwa aktualnie zalogowanego użytkownika: "
echo "$USER"
echo

echo "Katalog domowy użytkownika: "
echo "$HOME"
echo
