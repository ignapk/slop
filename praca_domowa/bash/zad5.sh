#!/usr/bin/bash

function zamien(){
	read -a tekst

	index=0
	while [ $index -lt ${#tekst[@]} ]; do
		if [ "${tekst[$index]}" = "$1" ]; then
			tekst[$index]="$2"
		fi

		index=$(( $index + 1 ))
	done
	
	echo "${tekst[@]}"
}

echo "Ala ma kota" | zamien kota psa
