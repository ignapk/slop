#!/usr/bin/bash

if [ "$1" = "create" ]; then
	if [ -z "$2" ]; then
		echo "Usage: $0 create <filename>"
	else
		touch "$2"
		echo "Created file $2"
	fi
elif [ "$1" = "delete" ]; then
	if [ -z "$2" ]; then
		echo "Usage: $0 delete <filename>"
	else
		rm "$2"
		echo "Deleted file $2"
	fi
elif [ "$1" = "copy" ]; then
	if [ -z "$2" ] || [ -z "$3" ]; then
		echo "Usage: $0 copy <filename1> <filename2>"
	else
		cp "$2" "$3"
		echo "Copied file $2 to $3"
	fi
elif [ "$1" = "move" ]; then
	if [ -z "$2" ] || [ -z "$3" ]; then
		echo "Usage: $0 move <filename1> <filename2>"
	else
		mv "$2" "$3"
		echo "Moved file $2 to $3"
	fi
elif [ "$1" = "directory" ]; then
	if [ -z "$2" ]; then
		echo "Usage: $0 directory <dirname>"
	else
		mkdir "$2"
		echo "Created directory $2"
	fi
else
	echo "Usage: $0 <create | delete | copy | move | directory> <args...>"
fi
