#!/usr/bin/bash

read -p "Enter command: " cmd

if [ "$cmd" = "create" ]; then
	read -p "Enter filename: " file1
	if [ -z "$file1" ]; then
		echo "Filename cannot be empty"
	else
		touch "$file1"
		echo "Created file $file1"
	fi
elif [ "$cmd" = "delete" ]; then
	read -p "Enter filename: " file1
	if [ -z "$file1" ]; then
		echo "Filename cannot be empty"
	else
		rm "$file1"
		echo "Deleted file $file1"
	fi
elif [ "$cmd" = "copy" ]; then
	read -p "Enter first filename: " file1
	read -p "Enter second filename: " file2
	if [ -z "$file1" ] || [ -z "$file2" ]; then
		echo "Filenames cannot be empty"
	else
		cp "$file1" "$file2"
		echo "Copied file $file1 to $file2"
	fi
elif [ "$cmd" = "move" ]; then
	read -p "Enter first filename: " file1
	read -p "Enter second filename: " file2
	if [ -z "$file1" ] || [ -z "$file2" ]; then
		echo "Filenames cannot be empty"
	else
		mv "$file1" "$file2"
		echo "Moved file $file1 to $file2"
	fi
elif [ "$cmd" = "directory" ]; then
	read -p "Enter dirname: " file1
	if [ -z "$file1" ]; then
		echo "Dirname cannot be empty"
	else
		mkdir "$file1"
		echo "Created directory $file1"
	fi
else
	echo "Command should be one of following: 
	create
	delete
	copy
	move
	directory
	"
fi
