#!/bin/sh
# $Id: fselect2-stdout,v 1.9 2020/11/26 00:09:12 tom Exp $
# Wybrałem fselect2-stdout z folderu /usr/share/doc/dialog/samples, gdyż jest najbardziej minimalistyczny z dostępnych przykładów zapisywania pliku

returntext=`dialog --stdout --title "Please choose a file" "$@" --fselect "./" 0 0`
returncode=$?

case ${returncode:-0} in
  0)
    ;;
  *)
    exit
    ;;
esac

exec 1>"$returntext"

echo "Informacja o procesorze: "
cat /proc/cpuinfo
echo

echo "Informacja o jądrze: "
uname -a
echo

echo "Nazwa aktualnie zalogowanego użytkownika: "
echo "$USER"
echo

echo "Katalog domowy użytkownika: "
echo "$HOME"
echo
