#!/usr/bin/bash

srcdir=$(dialog --stdout --title "Choose source directory:" --fselect "./" 0 0)
if [ $? -ne 0 ]; then
	exit
fi

args=""
i=0
files=()
for f in $(ls -p $srcdir | grep -v '/$')
do
	i=$(( i + 1 ))
	files[i]="$f"
	args+="$i \"$f\" off "
done
choices=$(dialog --stdout --checklist "Choose files:" 10 60 $i $args) || exit

destdir=$(dialog --stdout --title "Choose dest directory:" --fselect "./" 0 0)
if [ $? -ne 0 ]; then
	exit
fi

counter=0
step=$(( 100 / i ))
for choice in $choices
do
	echo $counter | dialog --gauge "Progress" 8 40
	cp "${srcdir}/${files[$choice]}" "$destdir"
	sleep 1
	counter=$(( counter + step ))
done
