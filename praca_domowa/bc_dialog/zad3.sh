#!/usr/bin/bash

exec 3>&1
cmd=`dialog --inputbox "Enter command:" 8 40 2>&1 1>&3`
exec 3>&-
if [ "$cmd" = "create" ]; then
	exec 3>&1
	file1=`dialog --inputbox "Enter filename:" 8 40 2>&1 1>&3`
	exec 3>&-
	if [ -z "$file1" ]; then
		dialog --title "Error" --msgbox "Filename cannot be empty" 5 30
	else
		touch "$file1"
		dialog --title "Success" --msgbox "Created file $file1" 5 30
	fi
elif [ "$cmd" = "delete" ]; then
	exec 3>&1
	file1=`dialog --inputbox "Enter filename:" 8 40 2>&1 1>&3`
	exec 3>&-
	if [ -z "$file1" ]; then
		dialog --title "Error" --msgbox "Filename cannot be empty" 5 30
	else
		rm "$file1"
		dialog --title "Success" --msgbox "Deleted file $file1" 5 30
	fi
elif [ "$cmd" = "copy" ]; then
	exec 3>&1
	file1=`dialog --inputbox "Enter first filename:" 8 40 2>&1 1>&3`
	exec 3>&-
	exec 3>&1
	file2=`dialog --inputbox "Enter second filename:" 8 40 2>&1 1>&3`
	exec 3>&-
	if [ -z "$file1" ] || [ -z "$file2" ]; then
		dialog --title "Error" --msgbox "Filenames cannot be empty" 5 30
	else
		cp "$file1" "$file2"
		dialog --title "Success" --msgbox "Copied file $file1 to $file2" 5 30
	fi
elif [ "$cmd" = "move" ]; then
	exec 3>&1
	file1=`dialog --inputbox "Enter first filename:" 8 40 2>&1 1>&3`
	exec 3>&-
	exec 3>&1
	file2=`dialog --inputbox "Enter second filename:" 8 40 2>&1 1>&3`
	exec 3>&-
	if [ -z "$file1" ] || [ -z "$file2" ]; then
		dialog --title "Error" --msgbox "Filenames cannot be empty" 5 30
	else
		mv "$file1" "$file2"
		dialog --title "Success" --msgbox "Moved file $file1 to $file2" 5 30
	fi
elif [ "$cmd" = "directory" ]; then
	exec 3>&1
	file1=`dialog --inputbox "Enter dirname:" 8 40 2>&1 1>&3`
	exec 3>&-
	if [ -z "$file1" ]; then
		dialog --title "Error" --msgbox "Dirname cannot be empty" 5 30
	else
		mkdir "$file1"
		dialog --title "Success" --msgbox "Created directory $file1" 5 30
	fi
else
	dialog --title "Error" --msgbox "Command should be one of following: 
	create
	delete
	copy
	move
	directory
	" 15 30
fi
