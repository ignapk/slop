#!/usr/bin/bash

: "${DIALOG_OK=0}"
: "${DIALOG_CANCEL=1}"

backtitle="Exercise 2 Form:"

name="Ignacy"
lastname="Kuchciński"
email="ignacykuchcinski@gmail.com"
user=$USER
password="tajne"
hobby="Linux"

returncode=0
while test $returncode != 1 && test $returncode != 250
do
exec 3>&1
returntext=`dialog --ok-label "Submit" \
	  --backtitle "$backtitle" \
	  --insecure "$@" \
	  --form "Here is a form for exercise 2." \
20 50 0 \
	"Imię:"		1 1 "$name"	1 10 40 0 \
	"Nazwisko:"	2 1 "$lastname"	2 10 40 0 \
	"Adres E-mail:"	3 1 "$email"	3 10 40 0 \
	"Login:"	4 1 "$user"	4 10 40 0 \
	"Hasło:"	5 1 "$password"	5 10 40 0 \
	"Hobby:"	6 1 "$hobby"	6 10 40 0 \
2>&1 1>&3`
returncode=$?
exec 3>&-

	case $returncode in
	$DIALOG_CANCEL)
		dialog \
		--clear \
		--backtitle "$backtitle" \
		--yesno "Really quit?" 10 30
		case $? in
		$DIALOG_OK)
			break
			;;
		$DIALOG_CANCEL)
			returncode=99
			;;
		esac
		;;
	$DIALOG_OK)
		exec 3>&1
		returnfile=`dialog \
			  --fselect ./ 10 40 \
			  2>&1 1>&3`
		returncode=$?
		exec 3>&-

		case $returncode in
		$DIALOG_OK)
			echo "$returntext" >> "$returnfile"
			;;
		$DIALOG_CANCEL)
			returncode=99
			;;
		esac

		;;
	*)
		exit
		;;
	esac
done
