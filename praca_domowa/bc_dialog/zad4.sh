#!/usr/bin/bash

limit=$( echo "limits" | bc -l | grep SCALE)
scale=$( dialog --stdout --inputbox "Limit: ${limit}\nEnter scale:" 8 40 )
if [ $? -ne 0 ]; then
	exit
fi

choice=$( dialog --stdout --menu "Oblicz:" 10 30 3 1 wielomian 2 pi 3 phi )
if [ $? -ne 0 ]; then
	exit
fi

case $choice in
1)
dialog --title "Wielomian" --msgbox $(echo "scale=$scale;0" | bc -l) 5 30
;;
2)
dialog --title "Pi" --msgbox $(echo "scale=$scale;4*a(1)" | bc -l) 5 30
;;
3)
dialog --title "Phi" --msgbox $(echo "scale=$scale;(1+sqrt(5))/2" | bc -l) 5 30
;;
esac
