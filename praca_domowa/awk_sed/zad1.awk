#!/usr/bin/awk -f

BEGIN { FS="\"*,\"*"; ile = 0 }

$1 == "\"female" && $6+0 > 60 { ile = ile + 1 }

END { print ile }

BEGIN { FS="\",\""; c = 0; d = 0; lc = 0; ld = 0 }

$1 == "\"male" { c = c + $7; ++lc }
$1 == "\"female" { d = d + $7; ++ld }
{ ++a }

END { print c/lc " " d/ld }

BEGIN { FS="\"*,\"*"; maxamr = 0; minamr = 100; maxamw = 0; minamw = 100; maxafr = 0; minafr = 100; maxafw = 0; minafw = 100; }

# objaśnienie nazw zmiennych - max/min oznacza najlepszy/najgorszy, następna litera oznacza grupę, następna płeć, następna przedmiot, np. maxamr = najlepszy wnik grupy A chłopców z czytania, poniżej rozwiązanie dla samej grupy A:

$2 == "group A" && $1 == "\"male" { if ($7+0 > maxamr) maxamr = $7; if ($7+0 < minamr) minamr = $7; if ($8+0 > maxamw) maxamw = $8; if ($8+0 < minamw) minamw = $8; }
$2 == "group A" && $1 == "\"female" { if ($7+0 > maxafr) maxafr = $7; if ($7+0 < minafr) minafr = $7; if ($8+0 > maxafw) maxafw = $8; if ($8+0 < minafw) minafw = $8; }

END { print maxamr " " minamr " " maxamw " " minamw " " maxafr " " minafr " " maxafw " " minafw }

BEGIN { ile2 = 0 }

$1 == "\"male" && ($6 == "100" || $6 == 100 || $7 == 100) { ile2 = ile2 + 1 }

END { print ile2 }
