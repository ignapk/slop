#!/bin/bash

# Zadanie 1.

wget http://ftp.slackware.pl/pub/slackware/slackware64-15.0/patches/FILE_LIST
cat FILE_LIST | grep -v kernel | grep \\.\/packages\/.*\\.txz$ | cut -d'/' -f3- > remote
find /var/lib/pkgtools/packages -type f | grep -v kernel | cut -d'/' -f6 | rev | cut -d'-' -f4- | rev > local

mkdir /root/patches

for local_pkg in $(cat local); do for remote_pkg in $(cat remote); do tmp=$(echo $remote_pkg | rev | cut -d'-' -f4- | rev); if [ $tmp = $local_pkg ]; then wget -P /root/patches http://ftp.slackware.pl/pub/slackware/slackware64-15.0/patches/packages/$remote_pkg; break; fi; done; done;

wget http://ftp.slackware.pl/pub/slackware/slackware64-15.0/patches/packages/mozilla-firefox-115.11.0esr-x86_64-1_slack15.0.txz

for pkg in /root/patches/*.txz; do upgradepkg $pkg; done

installpkg mozilla-firefox-115.11.0esr-x86_64-1_slack15.0.txz

rm mozilla-firefox-115.11.0esr-x86_64-1_slack15.0.txz local remote FILE_LIST

savechanges /run/initramfs/memory/data/slax/modules/zad1.sb

# Zadanie 2.

ln -sf /usr/share/zoneinfo/Europe/Warsaw /etc/localtime

# Zadanie 3.

userdel guest -r
useradd -m ignapk
sed -i '/google-chrome/d' /root/.fluxbox/menu
rm /usr/share/applications/google-chrome.desktop

savechanges /run/initramfs/memory/data/slax/modules/zad3.sb

# Zadanie 4.

mkdir deps
wget -P deps http://ftp.slackware.pl/pub/slackware/slackware64-15.0/slackware64/d/make-4.3-x86_64-3.txz
wget -P deps http://ftp.slackware.pl/pub/slackware/slackware64-15.0/slackware64/d/guile-3.0.7-x86_64-1.txz
wget -P deps http://ftp.slackware.pl/pub/slackware/slackware64-15.0/slackware64/l/gc-8.0.6-x86_64-1.txz
wget -P deps http://ftp.slackware.pl/pub/slackware/slackware64-15.0/slackware64/d/gcc-g%2B%2B-11.2.0-x86_64-2.txz
wget -P deps http://ftp.slackware.pl/pub/slackware/slackware64-15.0/slackware64/d/gcc-11.2.0-x86_64-2.txz
wget -P deps http://ftp.slackware.pl/pub/slackware/slackware64-15.0/slackware64/d/kernel-headers-5.15.19-x86-2.txz
wget -P deps http://ftp.slackware.pl/pub/slackware/slackware64-15.0/slackware64/d/binutils-2.37-x86_64-1.txz
wget -P deps http://ftp.slackware.pl/pub/slackware/slackware64-15.0/slackware64/l/readline-8.1.002-x86_64-1.txz
wget -P deps http://ftp.slackware.pl/pub/slackware/slackware64-15.0/slackware64/d/flex-2.6.4-x86_64-5.txz
installpkg deps/*.txz

wget http://tph.tuwien.ac.at/~oemer/tgz/qcl-0.6.7.tgz
tar -xf qcl-0.6.7.tgz
rm qcl-0.6.7.tgz
cd qcl-0.6.7
sed -i 's/^PLOPT/#PLOPT/;s/^PLLIB/#PLLIB/' Makefile
make

mkdir -p zad4.sb/usr/lib/qcl
mkdir zad4.sb/usr/bin
make QCLDIR=$(pwd)/PACKAGE/usr/lib/qcl QCLBIN=$(pwd)/PACKAGE/usr/bin install

dir2sb zad4.sb
mv zad4.sb /run/initramfs/memory/data/slax/modules/

removepkg make guile gc gcc-g++ gcc kernel-headers binutils readline flex
cd ..
rm -r qcl-0.6.7 deps
