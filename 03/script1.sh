#!/usr/bin/bash

#sprawdzanie ustawien
echo "limits" | bc

echo

#podstawa liczb dla wejscia (ibase) i wyjscia (obase)
echo "ibase" | bc
echo "obase" | bc

#sprawdzanie dokladnosci
echo "scale" | bc
echo "scale" | bc -l

#proste operacje matematyczne
echo "2+4*5" | bc

echo

# pierwiastek
echo "sqrt(16)" | bc

echo

# potega
echo "5^2" | bc

#liczba eulera
echo "e(1)" | bc -l

#liczba eulera do 2 miejsca po przecinku
echo "scale=2;e(1)" | bc -l

echo

#sinus
echo "s(1.57)" | bc -l

#konwersja na dwojkowy
echo "obase=2;24" | bc

#problem 1
#echo "7^7^7" | bc
