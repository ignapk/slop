#!/usr/bin/bash

#proste okno dialogowe
#dialog --title "Wiadomość" --msgbox "Witaj" 5 30

# okno z przyciskiem YES NO
#dialog --title "Wiadomosc 2" --yesno "Lubisz okno dialogowe?" 6 30

# okno informacyjne
#dialog --infobox "Prosze czekac..." 10 30
#sleep 4

# okno do wprowadzania danych
#dialog --inputbox "Podaj swoje imie" 8 40 2>err$$

#okno do wyswietlania tekstu
#dialog --textbox /etc/profile 22 70

#okno z menu
#dialog --menu "Kolor:" 10 30 3 1 czerwony 2 zielony 3 niebieski 2>err$$

#okno z lista opci do wielokrotnego wyboru
#dialog --checklist "Wybierz podzespoły" 10 60 3 1 "CPU" off 2 "GPU" off 3 "RAM" on 2>output.txt

#okno z lista pojedynczego wyboru
#dialog --backtitle "Wybór notebooka" --radiolist "Wybierz producenta:" 10 40 4 1 "Asus" off 2 "Acer" off 3 "Dell" on 4 "Lenovo" off 2>output.txt

#okno do wprowadzania hasla
#dialog --title "Wprowadzenie hasla" --passwordbox "Podaj haslo" 8 40 2>haslo.txt

#okno wyboru pliku
#dialog --fselect /home/ignapk/Projects/slop/03/ 10 40 2>sciezka.txt

#okno z paskiem postepu
<<TEST
licznik=0
while [ $licznik -lt 100 ]
do
	licznik=$[ $licznik + 1 ]
	sleep 1
	echo $licznik | dialog --gauge "Postep" 8 40
done
TEST

#okno dialogowe do ustawiania czasu
#dialog --clear --timebox "Ustaw zegar" 3 8 16 18 59 2>godz.txt

#okno kalendarza
#dialog --calendar "Kalendarz" 0 0 13 03 2025 2>data.txt
